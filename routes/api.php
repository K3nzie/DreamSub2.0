<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Route::middleware('auth:api')->get('/latest', 'InternalApiController@latestEp');

Route::middleware('auth:api')->get('/animes', 'InternalApiController@getAnime');
Route::patch('/uploadAvatar', 'InternalApiController@uploadAvatar');
Route::middleware('auth')->get('/latest', 'InternalApiController@latestEp');
