<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//=============== MAIN\PUBLIC ROUTES ===================

 // ===== GET ROUTES ========

Route::get('/', 'HomeController@homepage')->name('home');
 
Auth::routes();

Route::get('/profile', 'HomeController@profile')->name('profile');
Route::patch('/profile', 'HomeController@profileUpdateSettings')->name('profile.update');

//Route::get('/home', 'HomeController@cp')->name('CP');

// Public animes page
Route::get('/anime', 'HomeController@animeMain')->name('anime');
Route::get('/anime/{show}', 'HomeController@showAnime')->name('anime.show');

// Public TV Shows page

Route::get('/tvshows', 'HomeController@tvShowsMain')->name('tvshows');
Route::get('/tvshows/{show}', 'HomeController@showTvShows')->name('tvshows.show');

// Public films page

Route::get('/films', 'HomeController@filmsMain')->name('films');
Route::get('/films/{show}', 'HomeController@showFilms')->name('films.show');

// Public OVAs page

Route::get('/ova', 'HomeController@ovaMain')->name('ova');
Route::get('/ova/{show}', 'HomeController@showOva')->name('ova.show');

// ====== POST ROUTES =========



//======= RESOURCES ==========


  // ADMIN


//======= ADMIN AREA ======== 
Route::group(['prefix' => 'cp'], function() {

Route::get('/', 'ControlPanelController@index')->name('CP');

Route::resource('anime', 'AnimeController');
Route::resource('film', 'FilmController');
Route::resource('tv-show', 'TvShowController');
Route::resource('oav', 'OavController');

});
//Route::get('/cp/animes', 'ControlPanelController@getAnime')->name('cp.anime.show');
// Anzichè fare un controller e una risorsa per ogni categorie che abbiamo
// mi è sembrato più logico farne uno unico per tutto quanto, e lasciare
// decidere cosa fare alla logica nel controller, in base alla
// variabile utilizzata  showtype
//Route::resource('cp/{showtype}/{show}', 'ShowController');

