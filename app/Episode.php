<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
	protected $fillable = ['number', 'name', 'slug'];

	// Uno a molti (inverso)
    public function show()
    {
        return $this->belongsTo('App\Show');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }
}
