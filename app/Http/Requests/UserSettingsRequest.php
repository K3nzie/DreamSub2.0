<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'string|max:255|different:name|nullable',
            'email' => 'string|email|max:255|unique:users|different:email|nullable',
            'password' => 'string|min:6|confirmed|different:password|nullable',
        ];
    }

    public function messages()
    {
        return [
            'username.string' => 'L\'username inserito non sembra essere una stringa valida',
            'username.max' => 'Hai superato il limite di caratteri consentito per l\'username',
            'email.email' => 'L\'email inserita non è in un formato valido (nome@provider.dominio)',
            'email.unique' => 'Questa email sembra essere già in utilizzo da un\'altro utente',
            'password.min' => 'La password deve avere una lunghezza minima di 6 caratteri',
            'password.confirmed' => 'Devi confermare la password per poterla modificare correttamente',
            'email.different' => 'L\'eamil che hai inserito è uguale a quella utilizzata, se desideri non modificarlo lascia il campo vuoto',
            'username.different' => 'L\'username inserito è uguale a quello utilizzato, se desideri non modificarlo lascia il campo vuoto'


        ];
    }
}
