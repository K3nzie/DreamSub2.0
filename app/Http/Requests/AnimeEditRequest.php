<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimeEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'image|mimes:jpg,jpeg,png|max:10000|nullable',
            'name' => 'string|max:255|nullable',
            'slug' => 'string|max:255|nullable',
            'plot' => 'string|max:10000|nullable',
            'audio' => 'string|max:3|nullable',
            'sub' => 'string|max:3|nullable',
            'points' => 'nullable',
            'genre' => 'integer|nullable',
            'country' => 'integer|nullable',
            'type' => 'integer|nullable',
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
