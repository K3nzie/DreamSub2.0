<?php

namespace App\Http\Middleware;

use Closure;

class CheckControlPanelLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->level() < 3) {
            return redirect()->route('home')->with('warning', 'Non sei autorizzato ad accede in quest\'area.');
        }

        return $next($request);
    }
}
