<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use Illumintate\Http\File;
use Illuminate\Support\Facades\Storage;

class InternalApiController extends Controller
{
    public function latestEp(Request $request) {

    	$latest = Show::with('episodes')->latest()->take(10)->get();
        return $latest;


    }

    public function getAnime (Request $request) {
		
    	$animes = Show::whereHas('type', function($query) {
			$query->where('slug', '=', 'anime'); 
		})->firstOrFail(); 
        
		return $animes;
    	



    }

    public function getFilms(Request $request) {

    	$show = Show::whereHas('type', function($query) { 
			$query->where('slug', '=', 'film'); 
		})->get();

    }

    public function getTvShows(Request $request) {
    	
    	$show = Show::whereHas('type', function($query) { 
			$query->where('slug', '=', 'serie-tv'); 
		})->get();

    }

    public function getOva(Request $request) {
    	
    	$show = Show::whereHas('type', function($query) { 
			$query->where('slug', '=', 'ova'); 
		})->get();

    }

    public function uploadAvatar(Request $request) {
        /*if($request->hasFile('file')){
            $image = Image::make($request->file('file'))->resize(150,150);
            //$name = str_slug($request->('filename') . '.' . $image->getClientOriginalExtension());
            $destination = 'avatars/';

            if(Storage::putFileAs('avatars', new File($image), $name)){
                return json_encode("SUCCESSO!");
            } else {
                return json_encode("FALLIMENTO!");
            }

        } else {
            return json_encode("La richiesta non ha il file");
        }*/
        $cose = $request->all();

        return "tutto apposto";
    }
}