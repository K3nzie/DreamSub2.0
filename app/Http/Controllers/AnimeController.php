<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use App\Type;
use App\Country;
use App\Genre;
use Carbon\Carbon;
use App\Http\Requests\AnimeEditRequest;

class AnimeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkLevel');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animes = Show::whereHas('type', function($query) {
            $query->where('slug', '=', 'anime');
        })->paginate(10);

        return view('admin.anime.index', compact('animes'));
        //return var_dump($animes->first()->type()->first()->name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($anime)
    {
        $anime = Show::where('slug', $anime)->firstorFail();
        Carbon::setLocale('it');
        return view('admin.anime.show', compact('anime'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($anime)
    {
        $anime = Show::where('slug', $anime)->first();
        $oldSlug = $anime->slug;
        //$type = Type::where('slug', 'anime')->first();
        $langs = ['eng','ita', 'jap'];
        $countries = Country::all(['id', 'name']);
        $types = Type::all(['id', 'name']);
        $genres = Genre::all(['id', 'name']);
        return view('admin.anime.edit', compact('anime', 'oldSlug', 'langs', 'countries', 'types', 'genres')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnimeEditRequest $request, $anime)
    {
        $show = Show::where('slug', $anime)->firstOrFail();
        $filtered = Array();
        $exclude = ['_token', '_method']; //escludi alcuni valori che non stiano utilizzando
    
        foreach($request->except($exclude) as $key => $value) {
            if(is_numeric($show->$key)){
            if($show->$key === (int)$value){

                $filtered[] = $key;
            }
           } else {

            if($show->$key === $value){

                $filtered[] = $key;
                }

            }
            
        }

        $filtered[] = '_token';
        $filtered[] = '_method';
        if($request->except($filtered) != null && count($request->except($filtered)) > 0) {
        $show->fill($request->except($filtered));
        $show->save();
        }

        return "fatto! controlla il db.";
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
