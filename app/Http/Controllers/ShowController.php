<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use App\Type;
use Carbon\Carbon;

class ShowController extends Controller
{
    public function __construct(Request $request) 
    {
        $this->middleware('auth');
        $this->middleware('checkLevel');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request, $type)
    {
        $type = Type::where('slug', $type)->first();
        $typeSlug = $type->slug;
        Carbon::setLocale('it');
        $animes = Show::where('type_id', $type->id)->paginate(10);  

        return view('admin.shows', compact('animes', 'typeSlug'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($type, $show)
    {
        $show = Show::where('slug', $show)->first();
        $type = Type::where('slug', $type)->first();
        $langs = ['eng','ita', 'jap'];
        $countries = County::all(['id', 'name']);
        $types = Type::all(['id', 'name']);

        return view('admin.show-edit', compact('show', 'type', 'langs', 'countries', 'types' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
