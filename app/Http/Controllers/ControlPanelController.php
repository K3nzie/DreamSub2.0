<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Show;
use Carbon\Carbon;

class ControlPanelController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->middleware('checkLevel');
    
    }

    public function index(Request $request) {

    	return view('admin.index');

    }

    public function getAnime() {

    }
}
