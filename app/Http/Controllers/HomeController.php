<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserSettingsRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function homepage()
    {
        $latest = Show::latest()->take(10);
        return view('main.homepage', compact('latest'));
    }

    public function cp()
    {
        return view('home');
    }

    public function animeMain()
    {
        $animes = Show::all();
        return view('main.anime', compact('animes'));
    }

    public function showAnime()
    {
        return view('main.anime_show');
    }

    public function profile(Request $request) {

        // Logicamente, solo gli utenti loggati possono visualizzare il loro profilo

        $this->middleware('auth');

        // Trova l'utente o fallisci
        $user = User::findOrFail($request->user()->id);
        // Setta la locale italiana su Carbon così da avere una formattazione corretta
        // per la conversione in diff
        Carbon::setLocale('it');
        // Ciappa l'avatar 
        $avatar = $user->avatar;
        // e il ruolo dell'utente
        $role = $user->roles()->first()->name;
        // La data di registrzione così com'è nel database (timestamp)
        $reg = $user->created_at;
        // DfH
        $registered = $reg->diffForHumans();
        // Solo la data (gg-mm-aaaa)
        $date_registered = $reg->toDateString();
        // Solo l'orario
        $time_registered = $reg->toTimeString();
        // Settaggio dei colori in base al ruolo
        // Temporaneo, verrà sostituito da un attrubuto nella tabella dei ruoli

        switch (str_slug($role)) {
            case 'admin':
                $color = 'admin';
                break;
            case 'da-convalidare':
                $color = 'convalida';
                break;
            case 'bannato':
                $color = 'bannato';
                break;
            case 'supervisore':
                $color = 'supervisore';
                break;
            case 'grafico':
                $color = 'grafico';
                break;
            case 'sviluppatore':
                $color = 'developer';
                break;
            case 'releaser':
                $color = 'releaser';
                break;
            case 'moderatore':
                $color = 'moderatore';
                break;
            default:
                $color = '';
                break;
            
        }
        // view
        return view('main.profile', compact('user', 'role', 'color', 'registered', 'date_registered', 'time_registered', 'avatar'));
    }

    public function profileUpdateSettings(UserSettingsRequest $request)
    {
            $user = User::find($request->user());
            /*if(Hash::check($request->input("password"), $user->password)){
                return redirect()->back()->with('errors', 'The password you provided is the same you are using now, please choose a new password.');
            } else {
                return $request->all();
            }*/
            return $request->all();
        
    }

    // Controllers per le pagine principali (pubbliche) del menù.
    // Prevedo in futuro di renderle dinamiche e gestibili 
    // tramite una tabella nel database, così da non
    // modificare direttamente le routes del sito

    public function tvShowsMain(Request $request) {


        return view('main.publicBrowser');



    }

}
