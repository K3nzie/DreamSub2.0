<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use Carbon\Carbon;

class TvShowController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkLevel'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tv_shows = Show::whereHas('type', function($query) {
            $query->where('slug', '=', 'tv-show');
        })->paginate(10);
        return view('admin.tvshow.index', compact('tv_shows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tv_show) 
    {
        $show = Show::where('slug', $tv_show)->firstorFail();
        Carbon::setLocale('it');
        return view('admin.tvshow.show', compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tv_show)
    {
        $show = Show::where('slug', $tv_show)->first();
        $oldSlug = $show->slug;
        return view('admin.tvshow.edit', compact('show', 'oldSlug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
