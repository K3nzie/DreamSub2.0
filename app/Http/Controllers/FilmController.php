<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Show;
use Carbon\Carbon;

class FilmController extends Controller
{
     public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkLevel'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $films = Show::whereHas('type', function($query) {
            $query->where('slug', '=', 'film');
        })->paginate(10);
        return view('admin.film.index', compact('films'));
        //return dd($films);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($film)
    {
        $ilfilm = Show::where('slug', $film)->firstorFail();
        Carbon::setLocale('it');
        return view('admin.film.show', compact('ilfilm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($film)
    {
        $ilfilm = Show::where('slug', $film)->first();
        $oldSlug = $ilfilm->slug;
        return view('admin.film.edit', compact('ilfilm', 'oldSlug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
