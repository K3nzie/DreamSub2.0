<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use Carbon\Carbon;

class OavController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('checkLevel'); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oavs = Show::whereHas('type', function($query) {
            $query->where('slug', '=', 'oav');
        })->paginate(10);
        return view('admin.oav.index', compact('oavs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($oav)
    {
        $oav = Show::where('slug', $oav)->firstorFail();
        Carbon::setLocale('it');
        return view('admin.oav.show', compact('oav'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($oav)
    {
        $oav = Show::where('slug', $oav)->first();
        $oldSlug = $oav->slug;
        return view('admin.anime.edit', compact('oav', 'oldSlug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
