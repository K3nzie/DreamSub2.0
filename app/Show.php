<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;

class Show extends Model
{
    use Searchable;

    protected $fillable = ['name', 'slug', 'plot', 'type', 'country', 'completed', 'audio', 'sub', 'points', 'published_year', 'end_year'];
 

    // Relazione uno a molti
    public function episodes()
    {
        return $this->hasMany('App\Episode');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }
    
}