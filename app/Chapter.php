<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    public function episodes()
    {
        return $this->hasMany('App\Episode');
    }

    public function type()
    {
        return $this->belongsTo('App\Show');
    }
}
