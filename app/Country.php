<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name', 'slug', 'iso-31661'];

    public function shows()
    {
        return $this->hasMany('App\Show');
    }

}
