<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = ['name', 'slug'];
	// Relazione molti a uno
    public function shows()
    {
        return $this->hasMany('App\Show');
    }
}
