<?php

use Illuminate\Database\Seeder;

class EpisodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shows = App\Show::all();
        foreach($shows as $show) {
        	factory(App\Episode::class, 200)->create([
        		'show_id' => $show->id,
        	]);
        }

    }
}
