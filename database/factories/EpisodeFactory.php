<?php

use Faker\Generator as Faker;

$factory->define(App\Episode::class, function (Faker $faker) {
	$name = $faker->name;
	$slug = str_slug($name);

    return [
        'number' => random_int(1,400),
        'name' => $name,
        'slug' => $slug,
        'show_id' => 1,
    ];
});
