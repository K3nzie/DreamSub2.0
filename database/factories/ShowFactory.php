<?php

use Faker\Generator as Faker;

$factory->define(App\Show::class, function (Faker $faker) {
	$name = $faker->name;
	$slug = str_slug($name);

    return [
        'name' => $name,
        'slug' => $slug,
        'plot' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dapibus finibus elit, sed finibus dui euismod vitae. Vivamus id molestie diam. Cras ullamcorper, magna sit amet porttitor ornare, ligula leo volutpat eros, sit amet pharetra ipsum orci eleifend velit. Donec laoreet semper nibh, in dapibus ligula imperdiet vitae. Maecenas faucibus imperdiet eros eget sagittis. Aenean eget justo neque. Morbi ultricies ultricies lorem in molestie. Nullam non nulla urna. Vivamus faucibus bibendum nisl at euismod. Mauris in vestibulum sapien, vel luctus tellus. Ut ac nisi nibh. Aliquam sollicitudin mi molestie tortor aliquam, vitae consequat mauris finibus. Suspendisse id ultrices tortor, eget luctus orci. Nulla non elit iaculis, congue dolor in, aliquam ex. Maecenas pulvinar nisi ac quam sollicitudin, at efficitur leo tempor. Cras ullamcorper pulvinar ex, vel interdum tortor rhoncus quis. Pellentesque vehicula risus sit amet commodo pulvinar. Curabitur eu odio in tortor feugiat tincidunt vitae vel leo. Donec condimentum accumsan lobortis. Cras fringilla tellus eget risus vulputate accumsan. Etiam pretium turpis nec nisi viverra fringilla.', 
        'completed' => random_int(0,1),
        'audio' => 'jap',
        'sub' => 'ita',
        'points' => 0,
        'genre_id' => random_int(1,4),
        'country_id' => random_int(1,250),
        'type_id' => 1,
    ];
});
