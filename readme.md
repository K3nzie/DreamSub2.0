<p align="center"><img src="logoDS.png"></p>

# DreamSub 2.0

DreamSub è una piattaforma di streaming di anime, serie tv e film.
Questo README serve a tenere traccia del lavoro, più un blocco note che un readme.

## Fatto
- Database
- Login \ Registrazione utente
- Area amministrativa
- Protezione area amministrativa
- Ruoli utenti
- Sicurezza basata sui livelli
- Barra navigazione con info utente
- Aggiungere le CRUD per episodi\serie\film nel pannello di controllo
- Completare l'area amministrativa
- Pagina profilo utente
- Form di customizzazione del profilo
- Carousel interattivo ultimi inseriti, in homepage

## Da Fare
- Permessi ruoli utenti
- Configurare il file storage per l'upload dgli episodi
- Implementare l'uploader degli episodi
- ~~Completare l'area amministrativa~~
- ~~Aggiungere le CRUD per episodi\serie\film nel pannello di controllo~~
- ~~Pagina profilo utente~~
- ~~Form di customizzazione del profilo~~
- ~~API interna per le richieste provenienti dagli scripts javascript~~ (50%)
- Browser anime\serie\film con filtri in Vue
- ~~Carousel interattivo ultimi inseriti, in homepage~~
- Calendario inserimenti
- Forum utenti \ supporto (tenuto in forse)
- Pagina del player video
- Player  custom
- Funzione aggiunta preferiti
- Funzione segui serie
- Rivisitazione UI per tutto il sito

# Clonare il repository

Se serve clonare questo repository per lavorare su una propria copia, basta eseguire questo semplice comando git dentro una cartella appositamente creata per contenere i files. (esempio *DreamSub2.0*) :

`git clone git@gitlab.com:K3nzie/DreamSub2.0.git`

## Ruoli utenti
I rouli sono impostati in un modo abbastanza organizzato, ogni ruolo ha un livello da 1 a 5, e ha determinati permessi, per esempio, un amministratore di livello 5 ha abilitati tutti i permessi, che nel caso di DreamSub potrebbero essere (ancora da rifinire) "può caricare episodi", "può inserire serie nuove", "può cancellare", "può bannare" e via dicendo, in questo modo, quando ho bisogno di fare un controllo di sicurezza su una determinata azione, recuperato l'utente, ho vari modi per poterlo fare, qualche esempio :

`if(Auth::check() && Auth::user()->level() > 3) {`
 
`if(Auth::check() && Auth::user()->can("uppare-episodio")) {`

`if(Auth::check() && Auth::user()->isAdmin()) {`

`if(Auth::check() && Auth::user()->isReleaser()) {`

`if(Auth::check() && Auth::user()->isHelper()) {`
 
in questo modo l'autorizzazione ad effettuare varie azioni è intuitiva e immaediata, momentaneamente i ruoli nel database sono i seguenti :

- Admin
- Sviluppatore 
- Supervisore
- Moderatore
- Grafico
- Releaser
- Da convalidare
- Utente
- Bannato


## Struttura del database
Per quanto riguarda il database, **molte** cose sono ancora da definire e correggere, ho preso spunto dal database originale di Joker, ancora, non ho utilizzato tutte le informazioni perchè appunto ancora non tutto è definito correttamente.

Strutturalmente l'idea è questa, ogni tipo di contenuto caricato, è uno **Show**, ogni show può essere di tipi diversi: Anime, Serie TV, Film, OAV, piuttosto che fare una tabella per ogni tipo, così direi che è molto meglio.

Queste sono le varie tabelle:

- shows
- episodes
- chapters (ancora da decidere se tenerla o no)
- genres (Shonen, Avventura, Azione ecc..)
- migrations (Questa è una tabella che serve a laravel, spiegherò poi la sua funzione)
- password_resets (abbastanza esplicativa)
- permissions
- roles
- permission_role
- permission_user
- role_user
- types
- users

Il tutto, come già detto, è grezzo e solo una prima versione di ciò che sarà , mancano ancora dati importanti di cui discutere con il team.

### Convenzioni da seguire
Perchè seguire delle convenzioni? Perchè sennò la struttura di un progetto sembrerà disorganizzata e casinista, una cosa fondamentale secondo me, soprattutto col database, è stabilire delle regole. Per esempio l'attuale database, è un po' un miscuglio di cose, ci sono tabelle in inglese, tabelle in italiano, chiavi con nomenclature strane, relazioni tra una colonna in inglese è una in italiano, insomma, la prima volta che l'ho visto mi ha un po' mandato in confusione. Ecco perchè, propongo di seguire le seguenzi regole :

- I nomi delle tabelle del database **devono** essere in *inglese*. Per quanto l'italiano sia una lingua bellissima, il mondo dell'informatica è stato sviluppato sulla lingua inglese, e troppe cose farebbero confusione se lasciate in italiano, si vedano i punti seguenti.
- I nomi delle tabelle devono essere al **plurale**. Ciò significa che se una tabella contiene i dati di ogni gatto (cat) presente nel cortile, la tabella sarà gatti (cats).
- Le relazioni molti a molti: come immagino sappiate le relazioni molti a molti si realizzano utilizzando una tabella di intermezzo (pivot) tra le due tabelle che si devono relazionare, per esempio, nel caso dell'attuale struttura (vedi sopra), un ruolo può avere più di un permesso associato ad esso e un permesso può essere collegato a più ruoli. Quindi, come la inseriamo? La tabella ruoli si chiama **roles**, quella permessi si chiama **permissions**, la tabella relazionale si chiamera **role_permission**, notare il singolare delle due tabelle con nome plurale. Questa è una convenzione standard usata da laravel di default (che, attenzione, può essere sovrascritta con i nomi che si vogliono) che ho praticamente adottato in ogni progetto.

*To be continued...*

