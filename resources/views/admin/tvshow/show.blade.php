 @extends('layouts.adminLayout')
@section('content')
<div class="spacer my-4"></div>
@include('components.back')
<div class="container-fluid">
	<div class="row">
		<div class="col-3 offset-2 mb-1">
			<a type="button" class="btn btn-primary btn-sm">Modifica</a>
			<a type="button" class="btn btn-primary btn-sm">Elimina</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 offset-1 ml-5"> <!-- start col-md-3 -->
			<div class="card mb-3">
			  <img class="card-img-top" src="http://via.placeholder.com/150x150" alt="Card image cap">
			  <div class="card-body">
			    <h5 class="card-title">{{ $show->name }}</h5>
			    <p class="card-text">Trama</p>
			    <p class="card-text">{{ $show->plot }}</p>
			  </div>
			  <ul class="list-group list-group-flush">
			    <li class="list-group-item">Titolo : <strong>{{ $show->name }}</strong></li>
			    <li class="list-group-item">Titolo alternativo : <strong>titolo alternativo</strong></li>
			    <li class="list-group-item">Genere : <strong>{{ $show->genre()->first()->name }}</strong></li>
			    <li class="list-group-item">Anno : <strong>anno - anno</strong></li>
			    <li class="list-group-item">Episodi : <strong>{{ count($show->episodes()) }}</strong></li>
			    <li class="list-group-item">Durata media epidosio : <strong>{{ count($show->episodes()) }}</strong></li>
			    <li class="list-group-item">Audio : <strong>{{ count($show->audio) }}</strong></li>
			    <li class="list-group-item">Sub : <strong>{{ count($show->sub) }}</strong></li>
			    <li class="list-group-item">Punti : <strong>{{ $show->points }}</strong></li>
			    <li class="list-group-item"><strong>Il tuo voto : VOTO</strong></li>
			    <li class="list-group-item">Aggiunto il : <strong>{{ $show->created_at->toDateString() }}</strong></li>
			  </ul>
			  <div class="card-body">
			    <a href="#" class="card-link">Card link</a>
			    <a href="#" class="card-link">Another link</a>
			  </div>
			</div>
		</div> <!-- end col-md-3 (card) -->
		<div class="col-md-7"> <!-- start col-md-9 -->
			<div class="card mb-3">
				<div class="card-header">Trama</div>
				<div class="card-body">
					<p class="card-text">{{ $show->plot }}</p>
				</div>
				<div class="card-footer">
					<p class=""></p>
				</div>
			</div>
			<div class="card mb-3">
				<div class="card-header">Reviews</div>
				<div class="card-body">
					<p class="card-text">In arrivo</p>
				</div>
				<div class="card-footer text-muted">
					Vedi qualcosa che dovrebbe essere corretto? <a href="#">Faccelo sapere</a>!
				</div>
			</div>
			<div class="card mb-3">
				<div class="card-header">Trailer</div>
				<div class="card-body">
					<p class="card-text">In arrivo</p>
				</div>
				<div class="card-footer text-muted">
					Vedi qualcosa che dovrebbe essere corretto? <a href="#">Faccelo sapere</a>!
				</div>
			</div>
			
		</div> <!-- end col -md-9 -->
	</div>
</div>
@endsection