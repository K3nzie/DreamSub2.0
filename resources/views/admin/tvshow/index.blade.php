@extends('layouts.adminLayout')
@section('content') 
<div class="spacer my-3"></div>

<div class="container"> 
	<div class="row">
		<div class="col-12">
			<h1>Le serie TV in archivio</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped table-hover table-dark">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nome</th>
			      <th scope="col">Paese</th>
			      <th scope="col">Trama</th>
			      <th scope="col">Audio</th>
			      <th scope="col">Sub</th>
			      <th scope="col">Completata</th>
			      <th scope="col">Punti</th>
			      <th scope="col">Genere</th>
			      <th scope="col">Aggiunto</th>
			      <th scope="col">Ultima modifica</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if($tv_shows->isEmpty())
			  	<tr><p>Non ci sono serie TV in archivio</p></tr>
			  	@endif
			  	@foreach($tv_shows as $show)
			  		
			    <tr>
			      <th scope="row">{{ $show->id }}</th>
			      <td>{{ $show->name }}</td>
			      <td>{{ $show->country()->first()->name }}</td>
			      <td>{{ str_limit($show->plot, 70, '...') }}</td>
			      <td>{{ $show->audio }}</td>
			      <td>{{ $show->sub }}</td>
			      <td>{{ ($show->completed) ? 'Si' : 'No' }}</td>
			      <td>{{ $show->points }}</td>
			      <td>{{ $show->genre()->first()->name }}</td>
			      <td><abbr title="{{ $show->created_at->toDateString() }}">{{ $show->created_at->diffForHumans() }}</abbr></td>
			      <td><abbr title="{{ $show->updated_at->toDateString() }}">{{ $show->updated_at->diffForHumans() }}</abbr></td>
			      <td><a href="{{ route('tv-show.show', ['tv_show' => $show->slug]) }}" class="btn btn-primary" type="button"><i class="fab fa-readme"></i> Visualizza</button></td>
			      <td><a href="{{ route('tv-show.edit', ['tv_show' => $show->slug]) }}" class="btn btn-danger" type="button"><i class="fas fa-pen-square"></i> Modifica</button></td>
			      <td><a href="#" class="btn btn-danger" type="button"><i class="fas fa-minus-circle"></i> Elimina</button></td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
		<div class="col-12">
			{{ $tv_shows->links() }}
		</div>

	</div>
</div>


@endsection