@extends('layouts.adminLayout')
@section('content')
<div class="spacer my-4"></div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Elenco Anime</h1>
			<hr>
		</div>
	</div>
	@foreach($animes->chunk(3) as $anime)
	<div class="card-deck mb-3"> <!-- /row - /card-deck -->
		@foreach($anime as $entry)
	<div class="card">
	<img class="card-img-top" src="http://via.placeholder.com/150x150" alt="Cover di {{ $entry->name }}">
	  <div class="card-body">
	    <h5 class="card-title">{{ $entry->name }}</h5>
	    <p class="card-text"><small class="text-muted">Trama</small></p>
	    <p class="card-text">{{ str_limit($entry->plot, 100, '...') }}</p>
	  </div>
	  <ul class="list-group list-group-flush">
	    <li class="list-group-item">Ultimo episodio aggiunto {{ $entry->episodes()->latest()->first()->created_at->diffForHumans() }}</li>
	    <li class="list-group-item">Titolo ultimo episodio : {{ $entry->episodes()->latest()->first()->name }}</li>
	    <li class="list-group-item">Punteggio : {{ $entry->points }}</li>
	  </ul>
	  <div class="card-body">
	    <a href="{{ route('cp.anime.show', ['type' => $typeSlug, 'show' => $entry->slug]) }}" class="card-link">Visualizza</a>
	    <a href="{{ route('cp.anime.edit', ['type' => $typeSlug, 'show' => $entry->slug]) }}" class="card-link">Modifica</a>
	    <a href="{{ route('cp.anime.delete', ['type' => $typeSlug, 'show' => $entry->slug]) }}" type="button" class="card-link">Elimina</a>
	  </div>
	</div>
  		@endforeach
	</div> <!-- end /row - end /card-deck -->
	@endforeach
	<div class="row">
		<div class="col-12 text-center">
			{{ $animes->links() }}
		</div>
	</div>

@endsection