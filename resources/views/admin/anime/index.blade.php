@extends('layouts.adminLayout')
@section('content') 
<div class="spacer my-3"></div>

<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Gli anime in archivio</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped table-hover table-dark">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nome</th>
			      <th scope="col">Paese</th>
			      <th scope="col">Trama</th>
			      <th scope="col">Audio</th>
			      <th scope="col">Sub</th>
			      <th scope="col">Completata</th>
			      <th scope="col">Punti</th>
			      <th scope="col">Genere</th>
			      <th scope="col">Aggiunto</th>
			      <th scope="col">Ultima modifica</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if($animes->isEmpty())
			  	<tr><p>Non ci sono anime in archivio</p></tr>
			  	@endif
			  	@foreach($animes as $anime)
			  		
			    <tr>
			      <th scope="row">{{ $anime->id }}</th>
			      <td>{{ $anime->name }}</td>
			      <td>{{ $anime->country()->first()->name }}</td>
			      <td>{{ str_limit($anime->plot, 70, '...') }}</td>
			      <td>{{ $anime->audio }}</td>
			      <td>{{ $anime->sub }}</td>
			      <td>{{ ($anime->completed) ? 'Si' : 'No' }}</td>
			      <td>{{ $anime->points }}</td>
			      <td>{{ $anime->genre()->first()->name }}</td>
			      <td><abbr title="{{ $anime->created_at->toDateString() }}">{{ $anime->created_at->diffForHumans() }}</abbr></td>
			      <td><abbr title="{{ $anime->updated_at->toDateString() }}">{{ $anime->updated_at->diffForHumans() }}</abbr></td>
			      <td><a href="{{ route('anime.show', ['anime' => $anime->slug]) }}" class="btn btn-primary" type="button"><i class="fab fa-readme"></i> Visualizza</button></td>
			      <td><a href="{{ route('anime.edit', ['anime' => $anime->slug]) }}" class="btn btn-danger" type="button"><i class="fas fa-pen-square"></i> Modifica</button></td>
			      <td><a href="#" class="btn btn-danger" type="button"><i class="fas fa-minus-circle"></i> Elimina</button></td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
		<div class="col-12">
			{{ $animes->links() }}
		</div>

	</div>
</div>


@endsection