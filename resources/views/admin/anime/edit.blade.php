@extends('layouts.adminLayout')
@section('content')
<div class="spacer my-4"></div>
@include('components.back')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Stai modificando <strong>{{ $anime->name }}</strong>, un <strong>{{ $anime->type()->first()->name }}</strong></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<form name="editAnime" method="post" action="{{ route('anime.update', ['anime' => $anime->slug]) }}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<input type="hidden" id="oldSlug" value="{{ $oldSlug }}">
				<div class="form-group">
					<img src="http://via.placeholder.com/150x150">
					<label>Immagine di copertina</label>
					<input type="file">

				</div>
				<div class="form-group">
					<label>Nome</label>
					<input type="text" class="form-control form-control-lg" placeholder="" value="{{ $anime->name }}" name="name">
				</div>
				<div class="form-group">
					<label>Slug</label>
					<input type="text" class="form-control form-control-lg" placeholder="" value="{{ $anime->slug }}" name="slug">
				</div>
				<div class="form-group">
					<label>Trama</label>
					<textarea name="plot" rows="25" class="form-control">{{ $anime->plot }}</textarea>
				</div>
				<div class="form-group">
					<div class="col-6">
						<label>Audio</label>
						<select name="audio" class="custom-select form-control">
						  <option value="">Scegli una lingua</option>
						  @foreach($langs as $lang)
						  @if($anime->audio === $lang)
						  <option value="{{ $lang }}" selected="selected">{{ strtoupper($lang) }}</option>
						  @else
						  <option value="{{ $lang }}">{{ strtoupper($lang) }}</option> 
						  @endif
						  @endforeach
						</select>
					</div>
					<div class="col-6">
						<label>Sub</label>
						<select name="sub" class="custom-select">
						  <option value="">Scegli una lingua</option>
						  @foreach($langs as $lang)
						  @if($anime->sub === $lang)
						  <option value="{{ $lang }}" selected="selected">{{ strtoupper($lang) }}</option>
						  @else
						  <option value="{{ $lang }}">{{ strtoupper($lang) }}</option> 
						  @endif
						  @endforeach
						</select>

					</div>
				</div>
				<div class="form-group">
					<div class="col-3">
						<label>Points</label>
						<input type="text" size="4" value="{{ ($anime->points == 0) ? sprintf('%.2f', $anime->points) : $anime->points }}" readonly>
					</div>
					<div class="col-9">
						<label>Genere</label>
						<select name="genre_id" class="custom-select form-control">
							<option value="">Scegli una genere</option>
						  @foreach($genres as $genre)
						  @if($anime->genre()->first()->id === $genre->id)
						  <option value="{{ $genre->id }}" selected="selected">{{ $genre->name }}</option>
						  @else
						  <option value="{{ $genre->id }}">{{ $genre->name }}</option> 
						  @endif
						  @endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-8">
						<label>Paese</label>
						<select name="country_id" class="custom-select form-control">
						  <option value="">Scegli una paese</option>
						  @foreach($countries as $country)
						  @if($anime->country()->first()->id === $country->id)
						  <option value="{{ $country->id }}" selected="selected">{{ $country->name }}</option>
						  @else
						  <option value="{{ $country->id }}">{{ $country->name }}</option> 
						  @endif
						  @endforeach
						</select>
					</div>
					<div class="col-4">
						<label>Tipo</label>
						<select name="type_id" class="custom-select form-control">
						  <option value="">Scegli una tipo</option>
						  @foreach($types as $type)
						  @if($anime->type()->first()->id === $type->id)
						  <option value="{{ $type->id }}" selected="selected">{{ $type->name }}</option>
						  @else
						  <option value="{{ $type->id }}">{{ $type->name }}</option> 
						  @endif
						  @endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-lg">Conferma Modifiche</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection