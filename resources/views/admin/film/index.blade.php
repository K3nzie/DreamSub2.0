@extends('layouts.adminLayout')
@section('content') 
<div class="spacer my-3"></div>

<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>I film in archivio</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped table-hover table-dark">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nome</th>
			      <th scope="col">Paese</th>
			      <th scope="col">Trama</th>
			      <th scope="col">Audio</th>
			      <th scope="col">Sub</th>
			      <th scope="col">Completata</th>
			      <th scope="col">Punti</th>
			      <th scope="col">Genere</th>
			      <th scope="col">Aggiunto</th>
			      <th scope="col">Ultima modifica</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if($films->isEmpty())
			  	<tr><p>Non ci sono film in archivio</p></tr>
			  	@endif
			  	@foreach($films as $film)
			  		
			    <tr>
			      <th scope="row">{{ $film->id }}</th>
			      <td>{{ $film->name }}</td>
			      <td>{{ $film->country()->first()->name }}</td>
			      <td>{{ str_limit($film->plot, 70, '...') }}</td>
			      <td>{{ $film->audio }}</td>
			      <td>{{ $film->sub }}</td>
			      <td>{{ ($film->completed) ? 'Si' : 'No' }}</td>
			      <td>{{ $film->points }}</td>
			      <td>{{ $film->genre()->first()->name }}</td>
			      <td><abbr title="{{ $film->created_at->toDateString() }}">{{ $film->created_at->diffForHumans() }}</abbr></td>
			      <td><abbr title="{{ $film->updated_at->toDateString() }}">{{ $film->updated_at->diffForHumans() }}</abbr></td>
			      <td><a href="{{ route('film.show', ['film' => $film->slug]) }}" class="btn btn-primary" type="button"><i class="fab fa-readme"></i> Visualizza</button></td>
			      <td><a href="{{ route('film.edit', ['film' => $film->slug]) }}" class="btn btn-danger" type="button"><i class="fas fa-pen-square"></i> Modifica</button></td>
			      <td><a href="#" class="btn btn-danger" type="button"><i class="fas fa-minus-circle"></i> Elimina</button></td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
		<div class="col-12">
			{{ $films->links() }}
		</div>

	</div>
</div>


@endsection