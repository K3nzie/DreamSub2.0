@extends('layouts.adminLayout')
@section('content')
<div class="spacer my-4"></div>
@include('components.back')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Stai modificando <strong>{{ $oav->name }}</strong>, un <strong>{{ $oav->type()->first()->name }}</strong></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<form name="editAnime" method="post" action="{{ route('oav.update', ['oav' => $oav->slug]) }}">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<input type="hidden" id="oldSlug" value="{{ $oldSlug }}">
				<div class="form-group">
					<img src="http://via.placeholder.com/150x150">
					<label>Immagine di copertina</label>
					<input type="file" name="image">

				</div>
				<div class="form-group">
					<label>Nome</label>
					<input type="text" class="form-control form-control-lg" placeholder="" value="{{ $oav->name }}" name="nome">
				</div>
				<div class="form-group">
					<label>Slug</label>
					<input type="text" class="form-control form-control-lg" placeholder="" value="{{ $oav->slug }}" name="slug">
				</div>
				<div class="form-group">
					<label>Trama</label>
					<textarea name="plot" rows="25" class="form-control">{{ $oav->plot }}</textarea>
				</div>
				<div class="form-group">
					<div class="col-6">
						<label>Audio</label>
						<select name="audio" class="custom-select form-control">
						  <option value="">Scegli una lingua</option>
						  <option value="ita">ITA</option>
						  <option value="jap">JAP</option>
						  <option value="eng">ENG</option>
						</select>
					</div>
					<div class="col-6">
						<label>Sub</label>
						<select name="sub" class="custom-select">
						  <option value="">Scegli una lingua</option>
						  <option value="ita">ITA</option>
						  <option value="jap">JAP</option>
						  <option value="eng">ENG</option>
						</select>

					</div>
				</div>
				<div class="form-group">
					<div class="col-3">
						<label>Points</label>
						<input type="text" size="4" name="punti" value="{{ $oav->points }}">
					</div>
					<div class="col-9">
						<label>Genere</label>
						<select name="genere" class="custom-select form-control">
						  <option value="">Scegli una genere</option>
						  <option value="ita">ITA</option>
						  <option value="jap">JAP</option>
						  <option value="eng">ENG</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-8">
						<label>Paese</label>
						<select name="paese" class="custom-select form-control">
						  <option value="">Scegli una tipo</option>
						  <option value="ita">ITA</option>
						  <option value="jap">JAP</option>
						  <option value="eng">ENG</option>
						</select>
					</div>
					<div class="col-4">
						<label>Tipo</label>
						<select name="tipo" class="custom-select form-control">
						  <option value="">Scegli una tipo</option>
						  <option value="ita">ITA</option>
						  <option value="jap">JAP</option>
						  <option value="eng">ENG</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-lg">Conferma Modifiche</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection