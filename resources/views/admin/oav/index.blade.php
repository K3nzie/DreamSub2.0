@extends('layouts.adminLayout')
@section('content') 
<div class="spacer my-3"></div>

<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Gli OAV in archivio</h1>
		</div>
		<div class="col-12">
			<table class="table table-striped table-hover table-dark">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nome</th>
			      <th scope="col">Paese</th>
			      <th scope="col">Trama</th>
			      <th scope="col">Audio</th>
			      <th scope="col">Sub</th>
			      <th scope="col">Completata</th>
			      <th scope="col">Punti</th>
			      <th scope="col">Genere</th>
			      <th scope="col">Aggiunto</th>
			      <th scope="col">Ultima modifica</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@if($oavs->isEmpty())
			  	<tr><p>Non ci sono OAV in archivio</p></tr>
			  	@endif
			  	@foreach($oavs as $oav)
			  		
			    <tr>
			      <th scope="row">{{ $oav->id }}</th>
			      <td>{{ $oav->name }}</td>
			      <td>{{ $oav->country()->first()->name }}</td>
			      <td>{{ str_limit($oav->plot, 70, '...') }}</td>
			      <td>{{ $oav->audio }}</td>
			      <td>{{ $oav->sub }}</td>
			      <td>{{ ($oav->completed) ? 'Si' : 'No' }}</td>
			      <td>{{ $oav->points }}</td>
			      <td>{{ $oav->genre()->first()->name }}</td>
			      <td><abbr title="{{ $oav->created_at->toDateString() }}">{{ $oav->created_at->diffForHumans() }}</abbr></td>
			      <td><abbr title="{{ $oav->updated_at->toDateString() }}">{{ $oav->updated_at->diffForHumans() }}</abbr></td>
			      <td><a href="{{ route('oav.show', ['oav' => $oav->slug]) }}" class="btn btn-primary" type="button"><i class="fab fa-readme"></i> Visualizza</button></td>
			      <td><a href="{{ route('oav.edit', ['oav' => $oav->slug]) }}" class="btn btn-danger" type="button"><i class="fas fa-pen-square"></i> Modifica</button></td>
			      <td><a href="#" class="btn btn-danger" type="button"><i class="fas fa-minus-circle"></i> Elimina</button></td>
			    </tr>
			    @endforeach
			  </tbody>
			</table>
		</div>
		<div class="col-12">
			{{ $oavs->links() }}
		</div>

	</div>
</div>


@endsection