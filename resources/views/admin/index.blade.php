@extends('layouts.adminLayout')
@section('content')
<div class="spacer my-4"></div>
<div class="container">
	<div class="row mb-3 mt-2">
		<h1>Pannello di Controllo</h1>
		<p><strong><span class="red">Attenzione</span> : Sei in un'area sensibile. Qualunque azione eseguita dal pannello di controllo è solitamente permanente, attenti a cosa fate. </strong></p>
	</div>
	<div class="row">
	
<div class="col-4">
	<div class="card cpCard" style="">
	  <div class="card-body">
	    <h5 class="card-title">Card title</h5>
	    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	    <a href="#" class="card-link">Card link</a>
	    <a href="#" class="card-link">Another link</a>
	  </div>
	</div>
</div>
<div class="col-4">
	<div class="card cpCard" style="">
	  <div class="card-body">
	    <h5 class="card-title">Card title</h5>
	    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	    <a href="#" class="card-link">Card link</a>
	    <a href="#" class="card-link">Another link</a>
	  </div>
	</div>
</div>
<div class="col-4">
	<div class="card cpCard" style="">
	  <div class="card-body">
	    <h5 class="card-title">Card title</h5>
	    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
	    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
	    <a href="#" class="card-link">Card link</a>
	    <a href="#" class="card-link">Another link</a>
	  </div>
	</div>
</div>
</div>
</div>



@endsection

