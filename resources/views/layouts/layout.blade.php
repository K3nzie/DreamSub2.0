<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="../../../../favicon.ico">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:image" content="logo.png">

    <title>{{ config('app.name') }}</title>


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

  <body>

      @include('components.mainNavBar')

      @yield('content')

    <footer class="mainFoot container-fluid">
      <div class="container">
       <div class="row">
        <div class="col-12">
          <p>Dreamsub è nato con lo scopo di creare una community di appassionati di anime. Le serie che troverete sul nostro sito non sono di nostra proprietà e noi non possediamo i diritti su di esse. La traduzione degli episodi e l'aggiunta di sottotitoli alle serie è effetuata dal team di fansub. Queste vengono poi messe sul nostro sito senza scopo di lucro. Ci impegniamo comunque a rimuovere il materiale presente licenziato in Italia che può danneggiare il lavoro di terzi.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <ul class="list-inline">
            <li class="list-inline-item"><a class="mainFooterLink" href="#">Home</a></li>&raquo;
            <li class="list-inline-item"><a class="mainFooterLink" href="#">Chi siamo?</a></li>&raquo;
            <li class="list-inline-item"><a class="mainFooterLink" href="#">Contatti</a></li>&raquo;
            <li class="list-inline-item"><a class="mainFooterLink" href="#">Termini di utilizzo</a></li>&raquo;
            <li class="list-inline-item"><a class="mainFooterLink" href="#">Privacy Policy</a></li>&raquo;
            <li class="list-inline-item"><a class="mainFooterLink" href="#">DMCA</a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          Copyright &copy; 2016-2018 Dreamsub All rights reserved. 
        </div>
      </div>
     </div>
    </footer>
      
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('body-scripts')
  </body>
</html>
