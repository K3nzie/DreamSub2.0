<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="../../../../favicon.ico">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:image" content="logo.png">

    <title>{{ config('app.name', 'DreamSub - Anime Sub ITA') }}</title>


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

  <body id="cp">
    <div class="sidebar">
      <ul class="sidebar-nav">
        <li class="sidebar-nav-item sidebar-brand">
          <strong>DreamSub CP</strong>
        </li>
        <li class="sidebar-nav-item">
          <a href="{{ route('anime.index') }}">Elenco Anime</a>
        </li>
        <li class="sidebar-nav-item">
          <a href="{{ route('tv-show.index') }}">Elenco Serie TV</a>
        </li>
        <li class="sidebar-nav-item">
          <a href="{{ route('film.index') }}">Elenco Film</a>
        </li>
        <li class="sidebar-nav-item sidebar-brand">
          <strong>DreamSub CP</strong>
        </li>
        <li class="sidebar-nav-item">
          <a href="{{ route('oav.index') }}">Elenco OAV</a>
        </li>
        <li class="sidebar-nav-item">
          <a href="#">Utenti</a>
        </li>
        <li class="sidebar-nav-item">
          <a href="#">Ruoli</a>
        </li>
        <li class="sidebar-nav-item sidebar-brand">
          <strong>DreamSub CP</strong>
        </li>
        <li class="sidebar-nav-item">
          <a href="#">Link SideNav</a>
        </li>
        
      </ul>
    </div>

      @include('components.mainNavBar')

      @yield('content')

      <div class="container">
      <footer class="row justify-items-center align-items-center panelFooter">
        <div class="col-12">
          <p><small>DreamSub Control Panel v0.0.1 - <a href="#">contatta un admin</a> - <a href="#">contatta un developer</a></small></p>
        </div>
      </footer>
      </div>


    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
