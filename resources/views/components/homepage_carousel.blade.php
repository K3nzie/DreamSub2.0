<div class="spacer my-4"></div>


<div class="container"><!-- container -->
  <div class="row">
    <div class="col-12">
      <h3 class="text-center">Ultimi inserimenti</h3>
    </div>
  </div>
  <div id="latestSlider">
  	<div class="card-deck" v-if="latest && latest.length" v-cloak> <!-- card deck -->
          <carousel-3d :controls-visible="true" :clickable="false">
            <slide v-for="(show, i) in latest" :index="i">
              <figure>
                <img src="storage/avatars/@{{ show.avatar }}">
                <figcaption>
                  <h4>@{{ show.name }}</h4>
                  <p>@{{ show.plot.substr(0,150)+'...' }}</p>
                  <ul class="list-inline">
                    <li class="list-inline-item">Episodi : @{{ show.episodes.length }}</li>
                    <li class="list-inline-item">Anno : <strong>@{{ show.completed ? show.published_year + ' - ' + show.end_year : show.published_year }}</strong> </li>
                  </ul>

                </figcaption>
              </figure>
            </slide>
          </carousel-3d>

        </div>
      </div>
  	</div> <!-- end card-deck -->
	</div>
</div><!-- end container -->
