<div class="container-fluid" id="mainSplash">
	<div class="container">
	<div class="row align-items-center">
		<div class="col-7">
			<h1 class="leadhead">Benvenuto su <strong class="tile">Dream</strong><strong class="or">Sub</strong></h1>
			<h1>Guarda anime sottotitolati in italiano.</h1>
			<h2>Senza limiti ne pubblicità</h2>
		</div>
		<div class="col-4 text-right">

			<h4 class="mt-3">Non sei un utente? <strong>Registrati</strong>!</h4>
			<div class="card">
				<div class="card-header">
				    Compila il modulo ed entra nella community!
				  </div>
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name</label>

        
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                      
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class=" control-label">Password</label>

                           
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class=" control-label">Confirm Password</label>

                        
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                       
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary btn-block btn-lg">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- end registration panel -->
		</div>
	</div> <!-- /.row -->
</div><!-- /.container -->
</div><!-- container-fluid -->




