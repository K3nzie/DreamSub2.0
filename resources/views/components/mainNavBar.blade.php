<nav class="navbar navbar-expand-md fixed-top dsNavbar">
      <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('anime') }}">Anime</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('tvshows') }}">Serie TV</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('films') }}">Film</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('ova') }}">OVA</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="">Community</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul>
      
        <ul class="navbar-nav nav-right align-items-center">
          @guest
          <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>
          @else
          @if(auth()->user()->level() >= 3)
            <li class="nav-item">
              <a class="nav-link adminLabel" href="{{ route('CP') }}">Control Panel</a>
            </li>
          @endif
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="http://placehold.it/150x150" class="rounded-circle profileCircle"> {{ Auth::user()->name }}</a>
            <div class="dropdown-menu" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="{{ route('profile') }}">
              <i class="fas fa-child"></i> 
              Profilo
            </a>
              <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <i class="fas fa-key"></i> <strong>Logout</strong>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
              </form>
              <a class="dropdown-item" href="{{ route('profile') }}">
              <i class="fas fa-cogs"></i> 
              Impostazioni
              </a>
            @endguest
              
            </div>
          </li>
          
        </ul>
      </div>
    </nav>