@extends('layouts.layout')
@section('content')
<div class="spacer my-md-3"></div>
<div class="container">
    <div class="row">
        <div class="col-lg-8 order-lg-2">
            @if($errors->any())
            @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
            {{ $error }}
            </div> 
            @endforeach
            @endif
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profilo</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#settings" data-toggle="tab" class="nav-link">Edit</a>
                </li>
            </ul>
            <div class="tab-content py-1">
                <div class="tab-pane active" id="profile">
                    <h5 class="my-1 {{ $color }}">{{ Auth::user()->name }} <span class="badge badge-danger ml-1"><i class="fa fa-eye"></i> Amministratore</span></h5>
                    <!--<h6 class="my-1"><span class="badge badge-danger"><i class="fa fa-eye"></i> Amministratore</span></h6>-->
                    <h6 class="mb-3">Registrazione <i class="far fa-calendar-alt"></i> <strong><abbr title="{{ $date_registered }} alle {{ $time_registered }}">{{ $registered }}</abbr></strong></h6>
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Ultime serie seguite</h5>
                            <table class="table table-sm table-hover table-striped">
                                <tbody>                                    
                                    <tr>
                                        <td>
                                            <strong>Abby</strong> joined ACME Project Team in <strong>`Collaboration`</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Gary</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Kensington</strong> deleted MyBoard3 in <strong>`Discussions`</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>John</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Skell</strong> deleted his post Look at Why this is.. in <strong>`Discussions`</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/row-->
                </div>
                <div class="tab-pane" id="settings">
                    <div class="spacer my-md-3"></div>
                    <form role="form" name="updateSettings" action="{{ route('profile.update') }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
 
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Nome</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" value="{{ Auth::user()->name }}" name="username">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="email" value="{{ Auth::user()->email }}" name="email">
                            </div>
                        </div>
                        <strong class="my-2 text-center">Modifica la tua password</strong>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Password</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" name="password" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Conferma password</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" name="password_confirmation" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <input type="submit" class="btn btn-primary" value="Save Modifiche">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center" id="userAvatar">
            <img src="{{ asset('storage/avatars/'. Auth::user()->avatar) }}" class="mx-auto img-fluid img-circle d-block" alt="L'avatar di {{ Auth::user()->name }}" id="imgUserAvatar"> 
            <h6 class="mt-2">Quì puoi modificare il tuo avatar</h6>
            <div class="alert alert-danger" role="alert" v-if="errors && errors.length">
                <ul>
                    <li v-for="error in errors"> @{{ error }}</li>
                </ul>
            </div>
            <span class="mb-1 lighterAlert">Dimensione massima file: 10MB</span>
                <input type="file" id="avatarUploadInput" name="avatar" class=""  @change="fileChanged($event)" :disabled="uploading">
                <label for="avatarUploadInput" class="avatarUploadLabel"><span class="uploadSpan">Scegli un file</span></label>
                <button type="button" class="btn btn-outline-primary btn-block" v-show="enabled" @click="uploadFile()">Carica</button>

        </div>
    </div>
</div>




@endsection