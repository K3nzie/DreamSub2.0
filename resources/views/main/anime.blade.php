@extends('layouts.layout')

@section('content')
<div class="spacer my-md-3"></div>
<div class="container">
@foreach($animes->chunk(4) as $anime)
	<div class="row mb-3">
	@foreach($anime as $entry)
		<div class="col-3">
			<div class="card" style="">
			  <img class="card-img-top" src="http://via.placeholder.com/350x150" alt="Card image cap">
			  <div class="card-body">
			    <h5 class="card-title">{{ $entry->name }}</h5>
			    <h6 class="card-subtitle mb-2 text-muted">audio : {{ $entry->audio }} - sub : {{ $entry->sub }}</h6>
			    <p class="card-text">{{ str_limit($entry->plot, 100, '...') }}</p>

			    <a href="{{ route('anime.show', ['show' => $entry->slug]) }}" class="btn btn-primary">Guarda</a>
			  </div>
			</div>
		</div>
	@endforeach
	</div>
@endforeach
</div>
@endsection