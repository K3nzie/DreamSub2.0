@extends('layouts.layout')

@section('content')

@include('components.homepage_carousel')
@if(session()->has("warning"))
<div class="container">
	<div class="row">
		<div class="col-12">
			<p class="red"><strong>ATTENZIONE :</strong> {{ session("warning") }}</p>
		</div>
	</div>
</div>
@endif
<div class="container">
	<div class="row">
		<div class="jumbotron text-center">
			<h1>HomePage</h1>
			<p>This is the HomePage of DreamSub</p>
		</div>
		
	</div>
</div>


@endsection

