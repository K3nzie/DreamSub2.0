const userAvatar = new Vue({
    el: '#userAvatar',
    data: {
    	file: null,
    	filename: '',
    	size: 0,
    	errors: [],
    	uploading: false,
    	enabled: false


    },
    mounted: function() {

    },
    computed: {
    	fileSize: function() {
      		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      		if (this.size == 0) return 'n/a';
      		var i = parseInt(Math.floor(Math.log(this.size) / Math.log(1024)));
      		if (i == 0) return this.size + ' ' + sizes[i];
      		return (this.size / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    	}
    },
    methods: {
    	fileChanged: function(event) {
    		this.errors = [];
    		this.file = event.target.files[0];
    		this.filename = this.file.name.substring(0, this.file.name.lastIndexOf('.')); // Togli l'estensione
    		this.size = this.file.size;
    		if(this.file.type != "image/jpeg" && this.file.type != "image/png") {
				this.errors.push("Il file scelto non sembra essere valido");    			

    		}
    		else if(this.file.size > 10000000) {
    			this.errors.push("La dimensione del file supera quella consentita");
    		} else {
    			this.enabled = true;
    			this.errors = [];
    		} // fine if else if else
    	}, // fine fileChanged
    	uploadFile: function() {
    		this.uploading = true;
    		let formData = new FormData();
    		formData.append("file", this.file);
    		formData.append("filename", this.filename);
    		formData.append("filesize", this.file.size);

    		axios.post('/api/uploadAvatar', formData)
    		.then(function(response) {
    			this.success = true;
    			console.log("UPLOAD EFFETTUATO CON SUCCESSO! RISPOSTA :\n");


    		})
    		.catch(function(error) {
    			alert("ERRORE :" + error);

    		});

    	} // fine uploadfile
    }
});