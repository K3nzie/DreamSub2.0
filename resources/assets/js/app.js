
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
require('vue-carousel-3d');

import { Carousel3d, Slide } from 'vue-carousel-3d';


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('carousel', require('./components/homepagecarousel.vue'));

const carousel = new Vue({
    el: '#carousel-cards',
    data: {
    	episodes: {
    		'title': '',
    		'serie':'',
    		'added': ''

    	}
}

});

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

// Vue.component('profile-avatar', require('./components/profileAvatar.vue'));

const userAvatar = new Vue({
    el: '#userAvatar',
    data: {
    	file: null,
    	filename: '',
    	size: 0,
    	errors: [],
    	uploading: false,
    	enabled: false


    },
    mounted: function() {

    },
    computed: {
    	fileSize: function() {
      		var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
      		if (this.size == 0) return 'n/a';
      		var i = parseInt(Math.floor(Math.log(this.size) / Math.log(1024)));
      		if (i == 0) return this.size + ' ' + sizes[i];
      		return (this.size / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    	}
    },
    methods: {
    	fileChanged: function(event) {
    		this.errors = [];
    		this.file = event.target.files[0];
    		this.filename = this.file.name.substring(0, this.file.name.lastIndexOf('.')); // Togli l'estensione
    		this.size = this.file.size;
    		if(this.file.type != "image/jpeg" && this.file.type != "image/png") {
				this.errors.push("Il file scelto non sembra essere valido");    			

    		}
    		else if(this.file.size > 10000000) {
    			this.errors.push("La dimensione del file supera quella consentita");
    		} else {
    			this.enabled = true;
    			this.errors = [];
    		} // fine if else if else
    	}, // fine fileChanged
    	uploadFile: function() {
    		this.uploading = true;
    		let formData = new FormData();
    		formData.append("file", this.file);
    		formData.append("filename", this.filename);
    		formData.append("filesize", this.file.size);

    		axios.post('/api/uploadAvatar', {'method': 'patch'}, formData)
    		.then(function(response) {
    			this.success = true;
    			console.log("UPLOAD EFFETTUATO CON SUCCESSO! RISPOSTA :\n");


    		})
    		.catch(function(error) {
    			alert("ERRORE :" + error.message);

    		});

    	} // fine uploadfile
    }
});

/*const animeUpdate = new Vue({
    el: '#updateAnime',
    data: {
        episodes: {
            'title': '',
            'slug':'',

        }
}

});*/

//Vue.component('show-card', require('./components/ShowCard.vue'));

const latestSlider = new Vue({
    el: '#latestSlider',
    data: {
        latest: new Array(),
        first_page_url: '',

        current_page: 0,
        last_page: 0,
        last_page_url: '',

        next_page: 0,
        next_page_url: '',
        per_page: 0,
        total: 0,
        },
        components: { Carousel3d,Slide },
        mounted: function () {
            axios.get('/api/latest')
            .then(function(response) {
                response.data.forEach( function(element, index) {
                    this.latest.push(element);
                }.bind(this)); // non rimuovere questa dichiarazione, rende accessibili gli elementi in data*/
                

                console.log("RISPOSTA : ");
                console.log(response.data);

            }.bind(this))
            .catch(function(error) {
                console.log("Errori...");
                console.log(error);
            }.bind(this))

        }

});